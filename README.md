# 温度监控
> 程序框架初始化

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/214011_f2abd7fc_428243.png "实时图形.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/213959_9864cc81_428243.png "实时数据.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/214025_d5d21f62_428243.png "参数配置.png")

# TODO
1. 参数配置保存，加载
2. 串口协议读取，存储本地
3. 历史数据条件查询
4. 折线图点位描述优化