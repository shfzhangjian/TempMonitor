package com.lucksoft.TempMonitor.serialPort;

import com.lucksoft.TempMonitor.charts.RealTimeChart;
import com.lucksoft.TempMonitor.ui.GridPanel;
import org.jfree.data.time.Millisecond;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class serialData extends Thread {
    private long randomNum()
    {
        long value = (long)(Math.random()*20+80);
        System.out.println("value==>"+value);
        return value;
    }

    public static  List<String> data = new ArrayList<String>();

    @Override
    public void run() {
        while (true) {
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟
            Date date=new Date();

            String str = "";
            date = new Date(date.getTime());
            str = sdf.format(date)+",";
            long value1 = randomNum();
            str += value1+",";
            str += randomNum()+",";
            str += randomNum()+"";

            data.add(str);

            RealTimeChart.timeSeries.add(new Millisecond(date), value1);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
