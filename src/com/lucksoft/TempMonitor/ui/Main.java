package com.lucksoft.TempMonitor.ui;

import com.lucksoft.TempMonitor.db.Data;

import javax.swing.*;

public class Main   {
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JTable tempGrid;
    private JButton QuitBtn;
    private JButton configBtn;
    private JPanel chartPanel;
    private JPanel tablePanel;
    private JLabel appLabel;

    public void setData(Data data) {
    }

    public void getData(Data data) {
    }

    public boolean isModified(Data data) {
        return false;
    }

    public static void main(String[] args) {
        JFrame jFrame= new JFrame("温度检测");
        JPanel rootPane=new Main().panel1;
        jFrame.setContentPane(rootPane);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setSize(800, 600);
        jFrame.setLocationRelativeTo(rootPane);//居中
        jFrame.setVisible(true);
    }
}
