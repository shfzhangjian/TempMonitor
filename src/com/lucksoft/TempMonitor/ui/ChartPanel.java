package com.lucksoft.TempMonitor.ui;

import com.lucksoft.TempMonitor.charts.RealTimeChart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;

public class ChartPanel  extends JPanel {
    private JFrame mainUI;
    public static RealTimeChart rtcp;

    public ChartPanel(JFrame mainUI) {
        super();
        this.mainUI = mainUI;
        this.setBackground(Color.RED);
        this.rtcp=new RealTimeChart("温度随机数","","温度");
        this.setLayout(new BorderLayout());
        this.add(rtcp,new BorderLayout().CENTER);
    }

}
