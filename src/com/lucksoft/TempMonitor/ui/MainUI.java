package com.lucksoft.TempMonitor.ui;

import com.lucksoft.TempMonitor.serialPort.serialData;
import com.sun.awt.AWTUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainUI extends JFrame implements ActionListener {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String appTitle = "温度监控程序";

    /**
     * 程序界面宽度
     */
    public static final int WIDTH = 1100;

    /**
     * 程序界面高度
     */
    public static final int HEIGHT = 820;

    //设置window的icon（自定义了一下Windows窗口的icon图标
    Toolkit toolKit = Toolkit.getDefaultToolkit();
    Image icon = toolKit.getImage(MainUI.class.getResource("computer.png"));

    int LOC_X = (toolKit.getScreenSize().width - WIDTH) / 2;
    int LOC_Y = (toolKit.getScreenSize().height - HEIGHT) / 2;

    private static Thread refreshChartThread = null;
    private static Thread refreshGridThread = null;
    private static serialData readDataThread = null;

    /**
     * 设置主界面相关参数
     */
    public void launchFrame() {

        //设定程序在桌面出现的位置

        this.setTitle(appTitle);	//设置程序标题
        this.setIconImage(icon);
        this.setBackground(Color.white);	//设置背景色

        this.addWindowListener(new WindowAdapter() {
            //添加对窗口状态的监听
            public void windowClosing(WindowEvent arg0) {
                //当窗口关闭时
                System.exit(0);	//退出程序
            }

        });

        //this.setResizable(false);	//窗口大小不可更改
        // 读取配置
        this.init();
        this.initComponents();
        this.pack();
        this.setSize(WIDTH, HEIGHT);
        this.setLocation(LOC_X, LOC_Y);
        this.setVisible(true);	//显示窗口


    }

    private String showStyle = "chart";
    private String runState = "off";

    // 界面控件
    private JButton btnQuit ;  // 退出按钮
    private JButton btnSetup;  // 配置按钮
    private JToggleButton btnRun; // 读取按钮
    private JToggleButton btnHis; // 历史按钮
    private JToggleButton btnChart ; // 实时图形
    private JToggleButton btnGrid ; // 实时数据

    private JPanel titlePanel ; // 标题区域
    private WorkPanel workPanel ; // 工作区域
    private JPanel buttonPanel; // 按钮区域
    private JLabel currLabel;
    /*
    private ButtonGroup buttonGroup;
    private JRadioButton radioChartButton; // 切换图形
    private JRadioButton radioGridButton ; // 切换表格
    */
    //

    /**
     *  绘制界面控件
     */
    private void initComponents() {
        /*----标题部分----*/
        titlePanel = new JPanel();
        titlePanel.setLayout(new BorderLayout());
        JLabel title = new JLabel(" 欢迎使用温度监控系统   ");
        title.setFont(new Font("楷体", Font.BOLD, 28));
        titlePanel.add(title,BorderLayout.WEST);

        currLabel = new JLabel("实时图形");
        currLabel.setFont(new Font("宋体", Font.ITALIC, 18));
        titlePanel.add(currLabel);

        /*----选择部分----
        buttonGroup = new ButtonGroup();        //用它来放下面的单选按钮
        radioChartButton = new JRadioButton("图形显示", true);
        radioGridButton = new JRadioButton("表格显示", false);
        buttonGroup.add(radioChartButton);
        buttonGroup.add(radioGridButton);
         radioChartButton.addActionListener(this);
        radioGridButton.addActionListener(this);

        radioChartButton.setBounds(260, 180, 50,30);
        radioGridButton.setBounds(340,180,50,30);

        titlePanel.add(radioChartButton);
        titlePanel.add(radioGridButton);
        */

        /*----中间区域部分----*/
        workPanel = new WorkPanel(this);
        getContentPane().add(workPanel, BorderLayout.CENTER);

        /*----按钮部分----*/
        buttonPanel  = new JPanel();
        buttonPanel.setBackground(Color.WHITE);
        btnChart = new JToggleButton("实时图形");
        btnGrid = new JToggleButton("实时数据");
        btnHis = new JToggleButton("历史数据");
        btnRun = new JToggleButton("启动监控");
        btnSetup = new JButton("配置参数");
        btnQuit = new JButton("退出程序");
        titlePanel.add(buttonPanel,BorderLayout.EAST);

        buttonPanel.add(btnRun);
        buttonPanel.add(btnChart);
        buttonPanel.add(btnGrid);
        buttonPanel.add(btnHis);
        buttonPanel.add(btnSetup);
        buttonPanel.add(btnQuit);
        btnChart.setSelected(true);

        btnGrid.addActionListener(this);
        btnChart.addActionListener(this);
        btnHis.addActionListener(this);
        btnRun.addActionListener(this);
        btnSetup.addActionListener(this);
        btnQuit.addActionListener(this);
        titlePanel.setBackground(Color.WHITE);
        getContentPane().add(titlePanel, BorderLayout.NORTH);
        //getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        //workPanel.changeUI(showStyle);


    }

    /**
     *  读取配置，加载参数文件
     */
    private void init() {

    }



    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource()==btnChart) {
            workPanel.changeUI("chart");
            showStyle = ("chart");
            currLabel.setText("实时图形");

            btnChart.setSelected(true);
            btnGrid.setSelected(false);
            btnHis.setSelected(false);
        }

        if (e.getSource()==btnGrid) {
            workPanel.changeUI("grid");
            showStyle = ("grid");
            currLabel.setText("实时数据");

            btnChart.setSelected(false);
            btnGrid.setSelected(true);
            btnHis.setSelected(false);
        }

        if (e.getSource()==btnHis) {
            workPanel.changeUI("his");
            showStyle = ("his");
            currLabel.setText("历史数据");

            btnChart.setSelected(false);
            btnGrid.setSelected(false);
            btnHis.setSelected(true);
        }

        if (e.getSource()==btnSetup) {
            SetupDialog dlg = new SetupDialog(this,true);

            dlg.show();
        }

        if (e.getSource()==btnRun) {

            runState = ("off".equals(this.runState)?"on":"off");
            btnRun.setLabel( "off".equals(this.showStyle)?"启动监控":"停止监控");
            if ("off".equals(runState)) {
                if (refreshChartThread!=null) {
                    refreshChartThread.suspend();
                }
                if (refreshGridThread!=null) {
                    refreshGridThread.suspend();
                }
                if (readDataThread!=null) {
                    readDataThread.suspend();
                }

            } else {
                if (refreshChartThread == null) {
                    refreshChartThread = (new Thread(ChartPanel.rtcp));
                    refreshChartThread.start();
                } else {
                    refreshChartThread.resume();
                }

                if (refreshGridThread == null) {
                    refreshGridThread = (new Thread(WorkPanel.gridPanel));
                    refreshGridThread.start();
                } else {
                    refreshGridThread.resume();
                }

                if (readDataThread == null) {
                    readDataThread = (new serialData());
                    readDataThread.start();
                } else {
                    readDataThread.resume();
                }
            }
        }

        if (e.getSource()==btnQuit) {
            System.exit(0);
        }
        /*
        if(e.getSource()==radioGridButton) {
            workPanel.changeUI("grid");
        }
        if(e.getSource()==radioChartButton){
            workPanel.changeUI("chart");
        }
         */
    }

}
