package com.lucksoft.TempMonitor.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;

public class SetupDialog extends JDialog implements ActionListener {

    private JPanel contentPanel ; // 工作区域
    private JPanel buttonPanel; // 按钮区域

    private JButton ok;
    private JButton cancel;

    Toolkit toolKit = Toolkit.getDefaultToolkit();
    Image icon = toolKit.getImage(MainUI.class.getResource("computer.png"));
    /**
     * 程序界面宽度
     */
    public static final int WIDTH = 360;

    /**
     * 程序界面高度
     */
    public static final int HEIGHT = 540;

    int LOC_X = (toolKit.getScreenSize().width - WIDTH) / 2;
    int LOC_Y = (toolKit.getScreenSize().height - HEIGHT) / 2;

    SetupDialog(MainUI parent, boolean modal) {
        super(parent, modal);
        this.setTitle("配置参数");
        this.setSize(WIDTH,HEIGHT);
        this.setLocation(LOC_X, LOC_Y);

        this.setLayout(new BorderLayout(50,50));
        this.setTitle("参数配置");

        /*----添加基本信息面板----*/
        JPanel panelS = new JPanel();
        //设置标签和文本框
        sno_label = new JLabel("初始温度");
        sno_textfield = new JTextField(10);
        name_label = new JLabel("时间");
        name_textfield = new JTextField(10);
        birth_label = new JLabel("溶解温度1");
        birth_textfield = new JTextField(10);
        course1_label = new JLabel("时间1");
        course1_textfield = new JTextField(10);
        course2_label = new JLabel("淬灭温度2");
        course2_textfield = new JTextField(10);
        course3_label = new JLabel("时间2");
        course3_textfield = new JTextField(10);
        course4_label = new JLabel("延伸温度3");
        course4_textfield = new JTextField(10);
        course5_label = new JLabel("时间3");
        course5_textfield = new JTextField(10);
        //此面板用于放上面的标签和文本框
        this.contentPanel = new JPanel();
        contentPanel.setLayout(new GridLayout(8, 2, 0, 6));   //采用网格布局,使得它们排列均匀
        contentPanel.add(sno_label);
        contentPanel.add(sno_textfield);
        contentPanel.add(name_label);
        contentPanel.add(name_textfield);
        contentPanel.add(birth_label);
        contentPanel.add(birth_textfield);
        contentPanel.add(course1_label);
        contentPanel.add(course1_textfield);
        contentPanel.add(course2_label);
        contentPanel.add(course2_textfield);
        contentPanel.add(course3_label);
        contentPanel.add(course3_textfield);
        contentPanel.add(course4_label);
        contentPanel.add(course4_textfield);
        contentPanel.add(course5_label);
        contentPanel.add(course5_textfield);

        //将此面板插入到窗体中
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        //向此面板中加入空的面板，其匀称
        getContentPane().add(new JPanel(), BorderLayout.NORTH);
        getContentPane().add(new JPanel(), BorderLayout.WEST);
        getContentPane().add(new JPanel(), BorderLayout.EAST);


        this.buttonPanel = new JPanel();
        this.ok = new JButton("确定");
        this.cancel = new JButton("取消");
        buttonPanel.add(ok);
        buttonPanel.add(cancel);

        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        ok.setBounds(60, 100, 60, 25);
        cancel.setBounds(140, 100, 60, 25);
        ok.addActionListener(this);
        cancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==ok) {

        }
        dispose();
    }


    JLabel sno_label;               //学号标签
    JLabel name_label;              //姓名标签
    JLabel birth_label;             //出生日期标签
    JLabel course1_label;           //数据结构课程标签
    JLabel course2_label;           //Java课程标签
    JLabel course3_label;           //计算机网络课程标签
    JLabel course4_label;           //数字逻辑课程标签
    JLabel course5_label;           //Linux课程标签

    JTextField sno_textfield;       //学号文本框
    JTextField name_textfield;      //姓名文本框
    JTextField birth_textfield;     //出生日期文本框
    JTextField course1_textfield;   //数据结构课程文本框
    JTextField course2_textfield;   //Java课程文本框
    JTextField course3_textfield;   //计算机网络课程文本框
    JTextField course4_textfield;   //数字逻辑课程文本框
    JTextField course5_textfield;   //Linux课程文本框

    private JButton YES;                //保存按钮
    private JButton NO;                 //取消按钮



    //清空文本框
    public void clear(){
        sno_textfield.setText("");
        name_textfield.setText("");
        birth_textfield.setText("");
        course1_textfield.setText("");
        course2_textfield.setText("");
        course3_textfield.setText("");
        course4_textfield.setText("");
        course5_textfield.setText("");
    }


}
