package com.lucksoft.TempMonitor.ui;

import com.lucksoft.TempMonitor.db.Data;
import com.lucksoft.TempMonitor.serialPort.serialData;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class GridPanel  extends JPanel implements Runnable {
    private JFrame mainUI;

    private TableModel dataModel;
    private JScrollPane scrollpane;
    private JTable table;


    public GridPanel(JFrame mainUI) {
        super();
        this.mainUI = mainUI;
        this.setBackground(Color.green);
        this.setLayout(new BorderLayout());
        dataModel = getTableModel();

        table = new JTable(dataModel);
        scrollpane = new JScrollPane(table);
        this.add(scrollpane);

        table.getColumnModel().getColumn(0).setPreferredWidth(60);
        table.getColumnModel().getColumn(0).setMaxWidth(60);
        table.getColumnModel().getColumn(0).setMinWidth(60);
        DefaultTableCellRenderer r = new DefaultTableCellRenderer();
        r.setHorizontalAlignment(JLabel.CENTER);
        r.setBackground(Color.LIGHT_GRAY);

        DefaultTableCellRenderer r1 = new DefaultTableCellRenderer();
        r1.setHorizontalAlignment(JLabel.CENTER);


        table.getColumnModel().getColumn(0).setCellRenderer(r);
        table.getColumnModel().getColumn(1).setCellRenderer(r1);
        table.getColumnModel().getColumn(2).setCellRenderer(r1);
        table.getColumnModel().getColumn(3).setCellRenderer(r1);
        table.getColumnModel().getColumn(4).setCellRenderer(r1);
    }

    //读取外部文件，每一行当做一条字符串存入List中
    public List<String> getData() {

        return serialData.data;
    }

    //使用public List<String> getData() 方法得到的List构建数据模型
    //此处使用的外部文件中，每一行的字符串用空格分成四个部分
    //例如，其中一行为：2013-03-18 11:50:55   传感器1    报警，对应表格的一行
    public AbstractTableModel getTableModel() {
        return new AbstractTableModel() {
            public String getColumnName(int col){
                return n[col];
            }
            String[] n = {"序号","采集时间","数值1","数值2","数值3"};
            public int getColumnCount() {
                return 5;
            }
            public int getRowCount() {
                return getData().size();
            }
            public Object getValueAt(int row, int col) {
                switch (col) {
                    case (0): {
                        return row + 1;
                    }
                    case (1): {
                        return getData().get(row).split(",", 0)[0];
                    }
                    case (2): {
                        return getData().get(row).split(",", 0)[1];
                    }
                    case (3): {
                        return getData().get(row).split(",", 0)[2];
                    }
                    default:
                        return getData().get(row).split(",", 0)[3];
                }
            }
        };
    }

    public void run() {
        while (true) {
            //每隔1秒钟更新JTable
            table.validate();
            table.updateUI();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
