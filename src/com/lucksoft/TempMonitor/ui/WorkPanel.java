package com.lucksoft.TempMonitor.ui;

import javax.swing.*;
import java.awt.*;

public class WorkPanel extends JPanel {
    private JFrame mainUI;
    private ChartPanel chartPanel;
    public static GridPanel gridPanel;
    public static HisDataPanel hisPanel;
    private CardLayout cl;



    public void changeUI(String panelName) {
        cl.show(this,panelName);
    }

    public WorkPanel(JFrame mainUI) {
        this.mainUI = mainUI;

        cl = new CardLayout();
        this.setLayout(cl);

        this.chartPanel = new ChartPanel(mainUI);
        this.gridPanel = new GridPanel(mainUI);
        this.hisPanel = new HisDataPanel(mainUI);

        this.add("chart", chartPanel);
        this.add("grid", gridPanel);
        this.add("his", hisPanel);
    }
}
