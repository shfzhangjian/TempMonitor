package com.lucksoft.TempMonitor.charts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import java.awt.*;

public class RealTimeChart extends ChartPanel implements Runnable
{
    public static TimeSeries timeSeries;
    private long value=0;

    public RealTimeChart(String chartContent, String title, String yaxisName)
    {
        super(createChart(chartContent,title,yaxisName));
    }

    private static JFreeChart createChart(String chartContent, String title, String yaxisName){
        //创建时序图对象

        timeSeries = new TimeSeries(chartContent, Millisecond.class);
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection(timeSeries);

        StandardChartTheme standardChartTheme=new StandardChartTheme("CN");
        //设置标题字体
        standardChartTheme.setExtraLargeFont(new Font("隶书",Font.BOLD,20));
        //设置图例的字体
        standardChartTheme.setRegularFont(new Font("宋书",Font.PLAIN,15));
        //设置轴向的字体
        standardChartTheme.setLargeFont(new Font("宋书",Font.PLAIN,15));
        //应用主题样式
        ChartFactory.setChartTheme(standardChartTheme);

        JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(title,"时间(秒)",yaxisName,timeseriescollection,true,true,false);
        jfreechart.setBorderVisible(false);
        jfreechart.setBackgroundPaint(Color.WHITE);
        jfreechart.setBackgroundImageAlpha(0.0f);


        XYPlot xyplot = jfreechart.getXYPlot();

        // 透明度
        xyplot.setForegroundAlpha(0.5f);
        // 背景全透明
        xyplot.setBackgroundAlpha(0.0f);
        // 去除背景边框线
        xyplot.setOutlinePaint(null);
        xyplot.setDomainGridlinePaint(Color.black);
        xyplot.setRangeGridlinePaint(Color.black);

        XYItemRenderer renderer = (XYItemRenderer) xyplot.getRenderer();  // 能够得到renderer对象，就可以使用此方法设置颜色

        renderer.setSeriesPaint(0, Color.BLUE); // 设置第一条折现的颜色，以此类推。

        //纵坐标设定  
        ValueAxis valueaxis = xyplot.getDomainAxis();
        //自动设置数据轴数据范围  
        valueaxis.setAutoRange(true);
        //数据轴固定数据范围 30s  
        valueaxis.setFixedAutoRange(10000D);

        valueaxis = xyplot.getRangeAxis();
        //valueaxis.setRange(0.0D,200D);

        return jfreechart;
    }

    public void run()
    {/*
        while(true)
        {
            try
            {
                //timeSeries.add(new Millisecond(), randomNum());
                //timeSeries.add(new Millisecond(), randomNum());
                Thread.sleep(300);
            }
            catch (InterruptedException e)  {   }
        }
        */
    }

    private long randomNum()
    {
        //System.out.println((Math.random()*20+80));
        return (long)(Math.random()*20+80);
    }
} 