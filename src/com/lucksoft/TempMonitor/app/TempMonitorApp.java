package com.lucksoft.TempMonitor.app;

import com.lucksoft.TempMonitor.ui.MainUI;

import java.awt.*;

public class TempMonitorApp {

    public static void main(String[] args) {

        new MainUI().launchFrame();
    }
}
